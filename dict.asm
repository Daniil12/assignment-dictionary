section .text

global find_word

extern string_equals

find_word:
    cmp rsi, 0
    jz .not_found
    push rsi
    add rsi, 8
    call string_equals
    pop rsi
    cmp rax, 0
    jnz .found
    mov rsi, [rsi]
    jmp find_word
.found:
    mov rax, rsi
    ret
.not_found:
    xor rax, rax
    ret
