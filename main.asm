%include 'colon.inc'
%include 'words.inc'
%include 'lib.inc'

section .bss
    buffer: resb 256

section .rodata
    hello_msg: db 'Enter key: ', 0
    error_msg: db 'Key not found', 0
    over_buffer_msg: db 'Long key', 0
  
global _start

section .text

extern find_word

_start:
    sub rsp, buffer
    mov rdi, hello_msg
    call print_string
    mov rdi, rsp
    mov rsi, buffer 
    call read_word
    cmp rdx, buffer
    jz .over_buffer
    mov rdi, rax
    mov rsi, element
    call find_word
    cmp rax, 0
    jnz .found
.not_found:
    mov rdi, error_msg
    call print_error
    call print_newline
    call exit
.found:
    mov rdi, rax
    add rdi, 8
    call string_length
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
    call exit
.over_buffer:
    mov rdi, over_buffer_msg
    call print_error
    call print_newline
    call exit

